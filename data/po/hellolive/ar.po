# 
# Translators:
# Mubarak Qahtani <abu-q76@hotmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-19 05:34+0000\n"
"Last-Translator: Mubarak Qahtani <abu-q76@hotmail.com>\n"
"Language-Team: Arabic (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: hellolive.html15, 29
msgid "Welcome"
msgstr "أرحبوا"

#: hellolive.html:25
msgid "Hello."
msgstr "مرحبًا."

#: hellolive.html:26
msgid "Thank you for downloading Ubuntu MATE."
msgstr ""

#: hellolive.html:28
msgid "The"
msgstr ""

#: hellolive.html:29
msgid ""
"application is your companion for getting started. Once installed, the "
"Software Boutique is available to install a selection of featured "
"applications to help you get the most out of your computing experience."
msgstr ""

#: hellolive.html:33
msgid "We hope you enjoy Ubuntu MATE."
msgstr ""

#: hellolive.html:35
msgid "Continue"
msgstr "اكمل"
